#ifndef __ROUND_H
#define __ROUND_H

extern int8_t gameState;

extern void resetPlayer( int p, int8_t isFirst );
extern int8_t playGame( int8_t demo );
extern int8_t iterGame();
extern void loadSprites();

#endif
