BITMAP ShieldImage       "sprites/ShieldImage.png"         NONE
SPRITE PlayerSprite      "sprites/PlayerSprite.png"        2 1 FAST
SPRITE PlrBlowupSprites  "sprites/PlrBlowupSprites.png"    2 1 FAST
SPRITE PlayerShotSpr     "sprites/PlayerShotSpr.png"       1 1 FAST
SPRITE SpriteSaucer      "sprites/SpriteSaucer.png"        3 1 FAST
SPRITE SpriteSaucerExp   "sprites/SpriteSaucerExp.png"     3 1 FAST
SPRITE ShotExploding     "sprites/ShotExploding.png"       1 1 FAST
SPRITE AlienExplode      "sprites/AlienExplode.png"        2 1 FAST
SPRITE SquiglyShot       "sprites/SquiglyShot.png"         1 1 FAST
SPRITE AShotExplo        "sprites/AShotExplo.png"          1 1 FAST
SPRITE PlungerShot       "sprites/PlungerShot.png"         1 1 FAST
SPRITE RollShot          "sprites/RollShot.png"            1 1 FAST
SPRITE AlienSprA         "sprites/AlienSprA.png"           2 1 FAST
SPRITE AlienSprB         "sprites/AlienSprB.png"           2 1 FAST
SPRITE AlienSprC         "sprites/AlienSprC.png"           2 1 FAST
SPRITE AlienSprCY        "sprites/AlienSprCY.png"          2 1 FAST
SPRITE AlienSprCYInv     "sprites/AlienSprCYInv.png"       2 1 FAST
